from hyperlink import URL
import pytest

from url_cleaner.cleaners import TaobaoMarketplace

@pytest.mark.parametrize(
    ('text_in', 'text_expected'),
    [
        (
            'https://market.m.taobao.com/app/idleFish-F2e/widle-taobao-rax/page-detail?wh_weex=true&wx_navbar_transparent=true&id=607406603918&ut_sk=1.XDa383z3ob0DAICFtW6ziOYS_21407387_1572491712270.Copy.detail.607297043957.3860681611&forceFlush=1',
            'https://2.taobao.com/item.htm?id=607406603918'
        )
    ]
)
def test_taobao_marketplace(text_in, text_expected):
    cleaner = TaobaoMarketplace()
    assert cleaner.clean(URL.from_text(text_in)).to_text() == text_expected
