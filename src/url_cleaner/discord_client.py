import datetime
import logging
import re
import sys
from typing import Sequence

import click
import discord
from discord.ext import commands

from url_cleaner.cleaners import clean_url


client = commands.Bot(command_prefix='$')
AUTODETECT = False
DEBUG_ECHO = False


logger = logging.getLogger(__name__)

DEFAULT_LOG_FORMAT = '%(levelname)s - %(asctime)s - %(filename)s:%(funcName)s:%(lineno) - %(message)s'
DEFAULT_LOG_DTFORMAT = '%Y-%m-%d %H:%M:%S'

_default_formatter = logging.Formatter(DEFAULT_LOG_FORMAT, DEFAULT_LOG_DTFORMAT)
_default_handler = logging.StreamHandler(sys.stdout)
_default_handler.setFormatter(_default_handler)


@client.event
async def on_ready():
    logger.info('Logged in as %s <%d>.', client.user.name, client.user.id)
    logger.info('Autodetect %s.', 'ENABLED' if AUTODETECT else 'DISABLED')


@client.command()
async def echo(ctx: discord.Message, *, text: str):
    """ Echo for debugging """
    logger.debug('Echo: %s', text)
    if DEBUG_ECHO:
        await ctx.send(text)


def _format_cleaned_urls(urls_cleaned: Sequence[str]) -> str:
    if urls_cleaned:
        return 'Here are your cleaned URLs:\n' + '\n'.join(urls_cleaned)
    else:
        return 'No valid URLs found'


@client.event
async def on_message(ctx: discord.Message):
    """ Autodetect URLs in message """
    if not AUTODETECT:
        return

    urls_cleaned = []
    for match in re.finditer(r'\bhttps?://\S*\b', ctx.content):
        if not match:
            continue
        url_original = match.group()
        url_cleaned = clean_url(url_original)
        urls_cleaned.append(url_cleaned)

    await ctx.channel.send(_format_cleaned_urls(urls_cleaned))


@client.command()
async def conv(ctx: discord.Message, *, url: str):
    """ Manual conversion command """
    logger.debug('Request for: %s', url)

    urls_original = url.lower().split()
    urls_cleaned = list(filter(None, map(clean_url, urls_original)))

    await ctx.channel.send(_format_cleaned_urls(urls_cleaned))


# TODO: what is the correct type annotation for this?
@client.event
async def on_error(event: str, *args, **kwargs):
    exc_type, _, _ = sys.exc_info()
    if issubclass(exc_type, commands.CommandNotFound):  # fails silently
        pass


def configure_default_logging(level=logging.INFO):
    """ Set sane defaults for logging """
    logger.setLevel(level)
    if _default_handler not in logger.handlers:
        logger.addHandler(_default_handler)


@click.command()
@click.argument('api_token')
@click.option('--autodetect', is_flag=True, default=False)
@click.option('--debug-echo', is_flag=True, default=False)
def cli(api_token: str, autodetect: bool, debug_echo: bool):
    configure_default_logging(level=logging.DEBUG if debug_echo else logging.INFO)

    global AUTODETECT, DEBUG_ECHO
    AUTODETECT = autodetect
    DEBUG_ECHO = debug_echo

    client.run(api_token)


if __name__ == '__main__':
    cli()
