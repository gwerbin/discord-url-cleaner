from abc import ABCMeta, abstractmethod
from typing import Optional, TypeVar, Union

from hyperlink import URL


class Cleaner(metaclass=ABCMeta):
    @abstractmethod
    def check(self, url: URL) -> bool:
        """ Check if this is a valid URL to be cleaned """
        pass

    @abstractmethod
    def clean(self, url: URL) -> URL:
        """ Clean the URL, assuming it is valid """
        pass


class TaobaoMarketplace(Cleaner):
    def check(self, url: URL) -> bool:
        return url.host in {'2.taobao.com', 'market.m.taobao.com'}

    def clean(self, url: URL) -> URL:
        item_id_lst = url.get('id')
        if not item_id_lst:
            raise ValueError('No item ID found, not a valid store link.')
        item_id = item_id_lst[0]
        return URL('https', '2.taobao.com', ['item.htm'], [('id', item_id)])


_cleaners = {
    'Taobao Marketplace': TaobaoMarketplace()
}


def dispatch_clean(url: URL) -> URL:
    for cleaner_name, cleaner in _cleaners.items():
        if cleaner.check(url):
            return cleaner.clean(url)
    else:
        return None


def clean_url(url: str) -> Optional[str]:
    return dispatch_clean(URL.from_text(url)).to_text()

