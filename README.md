# Installation

```bash
pip install https://gitlab.com/gwerbin/discord-url-cleaner/-/archive/master/discord-url-cleaner-master.tar
```

# Usage

## Discord bot

```bash
python -m url_cleaner.discord_client YOUR_API_TOKEN
```

See `python -m url_cleaner.discord_client --help` for more options

